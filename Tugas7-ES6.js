 console.log('====================');
console.log('Tugas ES6');
console.log('NO 1');
console.log('====================');

//Mengubah fungsi menjadi fungsi arrow
goldenFunction = () => {
     console.log("this is golden !!");
}
goldenFunction();

console.log('====================');
console.log('NO 2');
console.log('====================');

//sederhanakan menjadi object literal di ES6
    
function newFunction (firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  
   
newFunction("William", "Imoh").fullName()

//belum selesai


console.log('====================');
console.log('NO 3');
console.log('===================='); 

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const { firstName, lastName, destination, occupation, spell } = newObject;  

console.log(firstName, lastName, destination, occupation)

console.log('====================');
console.log('NO 4');
console.log('===================='); 

let combined = ["Will", "Chris", "Sam", "Holly", "Gill", "Brian", "Noel", "Maggie"]
console.log(combined)

console.log('====================');
console.log('NO 5');
console.log('===================='); 

var before = 
'Lorem glass dolor sit amet, consectetur adipiscing elit, earth do eiusmod tempor  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam';

console.log(before);
